import * as React from "react";
import { Panel } from "../../src";
export default {
  title: "Template/Panel",
  parameters: {
    background: [{ name: "Blue", value: "#001D6D", default: true }],
  },
};
// токен с ролью MANAGER_TELLER
const token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwiRlJDT1JQLU1BTkFHRVJfVEVMTEVSIl0sImlzcyI6ImZya2stYXV0aG9yaXphdGlvbi1zZXJ2aWNlIiwidHlwIjoiQmVhcmVyIiwicGVybWlzc2lvbnMiOlsicmVzdWx0cy5zYWxlc19udW1iZXJfcmF0aW5nLmFsbG93X2FsbCIsInJlc3VsdHMub3Blbl9hY2Nfc2hvd2Nhc2UuYWxsb3dfYWxsIiwicmVzdWx0cy5rcGlfY2FyZC5hbGxvd19hbGwiLCJsa19lbXAuY2xpZW50X2NhcmQuY3JlYXRlX2FwcGxfZHJrYiIsImxrX2VtcC5jbGllbnRfY2FyZC5lZGl0X2NsaWVudF9jYXJkX2F0dHIiLCJyZXN1bHRzLm51bV9vcGVuX2FjY190cF9hcmVhX21vay5hbGxvd19hbGwiLCJsa19lbXAuY3JtLmFsbG93X2FsbCIsImxrX2VtcC5jbGllbnRfY2FyZC52aWV3X2NsaWVudF9jYXJkX290aF90cCIsImxrX2VtcC5jbGllbnRfY2FyZC5zZWFyY2hfY2xpZW50X2NhcmQiLCJyZXN1bHRzLmtwaV9yYXRpbmcuYWxsb3dfYWxsIiwicmVzdWx0cy5tb2tfcHJpemVfY2FsYy5hbGxvd19hbGwiLCJyZXN1bHRzLmxlYWRzX3Nob3djYXNlLmFsbG93X2FsbCIsInJlc3VsdHMucHJvZHVjdF9zaG93Y2FzZS5hbGxvd19hbGwiLCJsa19lbXAuY2xpZW50X2NhcmQuY3JlYXRlX2NsaWVudF9jYXJkIiwibGtfbGVhZGVyLnRhc2tzLnZpZXdfdGFza19saXN0X2Fzc2lnbmVkX2J5X2VtcCIsInJlc3VsdHMuc2FsZXNfY2FyZC5hbGxvd19hbGwiLCJyZXN1bHRzLnJhdGluZ19zZWxlY3Rpb25fcGFuZWwuYWxsb3dfYWxsIiwidHJuLmFsbC5hbGxvd19hbGwiLCJsa19lbXAuY2xpZW50X2NhcmQudmlld19jbGllbnRfY2FyZF9vd25fdHAiLCJsa19sZWFkZXIudGFza3Mudmlld190YXNrX2xpc3RfYXNzaWduZWRfdG9fZW1wIiwibGtfZW1wLmNsaWVudF9jYXJkLmZpeF9hY3RfY2xpZW50X2NhcmQiLCJsa19sZWFkZXIudGFza3MudGFrZV90YXNrX2luX3dvcmsiLCJyZXN1bHRzLnJhdGluZ19oeXN0X3BhbmVsX3BlcmlvZC5hbGxvd19hbGwiLCJyZXN1bHRzLmNvbnZlcnNfZnVubmVsX3RhYmxlLmFsbG93X2FsbCIsImRncy5hbGwuYWxsb3dfYWxsIiwicmVzdWx0cy5saXN0X2dyb3VwLmFsbG93X2FsbCIsImxrX2VtcC5sZWFkcy5hbGxvd19hbGwiLCJsa19sZWFkZXIudGFza3MuY3JlYXRlX3Rhc2tfdG9feW91cnNlbGYiLCJsa19lbXAuY2xpZW50X2NhcmQuY2hlY2tfY2xfYmxhY2tfbGlzdCIsInJlc3VsdHMucmF0aW5nX2NhcmQuYWxsb3dfYWxsIiwicmVzdWx0cy5wZXJpb2RfcmF0aW5nX3BhbmVsLmFsbG93X2FsbCIsInJlc3VsdHMucmF0aW5nX3ZpZXdfcGFuZWwuYWxsb3dfYWxsIiwicmVzdWx0cy5yZXdhcmRzX2NhcmQuYWxsb3dfYWxsIl0sImF6cCI6ImJhY2tlbmQiLCJzY29wZSI6InByb2ZpbGUiLCJuYW1lIjoibWFuYWdlcl90ZWxsZXIgIGVtcDQgbGtzIiwiYXR0cmlidXRlcyI6eyJlbXBsb3llZV90eXBlIjoiRU1QTE9ZRUUiLCJ0cm1fY29kZSI6IiIsImRvbWFpbiI6InJlZ2lvbi52dGIucnUiLCJlbXBsb3llZV9udW1iZXIiOiIwMDAwNTAiLCJjb21wYW55IjoiIiwidHBfY29kZSI6Ilo1MjAzLTEwMCIsImxvZ2luIjoibWFuYWdlcl90ZWxsZXJfNF9sa3MiLCJyZWdpb24iOiIiLCJ0aXRsZSI6IiIsImRlcGFydG1lbnQiOiIiLCJpbmZvIjoiIn0sInNlc3Npb25fc3RhdGUiOiIzOTE4N2VmMi1lNTViLTQ2YTUtOTc2OC1iMzllZGFkODRjZDgiLCJleHAiOjE2MDM3NzY1NjYsImlhdCI6MTYwMzY5MDE2NiwiZW1haWwiOiJtYW5hZ2VyX3RlbGxlcl80X2xrc0B2dGIucnUiLCJqdGkiOiJhNGY2ODA3ZC0yYzkwLTQyNzktYTUwMS01NzUwMjg2ZmEyOGQifQ.jCmQNp7LBhbZ2nIcUmXRBcQfGCXc56PXCjpyjG8UVCg8PEx5jB6TWgt_zy8iPTCDI25zzPMHlEJB6gGyPUoFvlI5EqXEGHfvyVS9g9ChL1DyOi1a78PNCPw8eAMS9ihDrOxlYAuYh7GWv9il-KF9EnRGKwIOo7S0mOy0SoJroO8_o3vkSW6ErurMO3BW1jn5MrSDEZiRtzgrv6xW8VpB0UYy6sl44WkosgCIUX56YT1_5fdjxHZGd0tjTga52JSkLuObgQywt8N1Nc63XEJgWMixuTyCbmWQV0UJF4aF2Jt1RdqZtsd0VgEERmTotofGKs2L2wqnnxkZPmJ0SkTRIw";
export const Default = () => (
  <div>
    <Panel token={token} />
    <h1>123</h1>
    <ul>
      <li>first</li>
      <li>second</li>
      <li>third</li>
    </ul>
  </div>
);
