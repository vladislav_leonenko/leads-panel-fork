import * as React from "react";
import { AlertPanel } from "../../src";

export default {
  title: "Organisms/Alert panel",
  parameters: {
    background: [{ name: "Blue", value: "#001D6D", default: true }],
  },
};

const token = "";

export const Default = () => <AlertPanel token={token} />;
