import * as React from "react";
import { Toasts } from "../../src";
export default {
  title: "Organisms/Notification Panel",
  parameters: {
    background: [{ name: "Blue", value: "#001D6D", default: true }],
  },
};
const token = "";
export const Default = () => <Toasts token={token} />;
