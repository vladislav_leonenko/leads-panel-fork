export declare const leadCardsMockUnallocated: {
    count_all: number;
    count_overdue: number;
    data: {
        assignment_deadline: string;
        contact_person_name: string;
        contact_person_phone: null;
        feedback_fixation_deadline: string;
        lead_channel: string;
        lead_company_name: string;
        lead_created_date: string;
        lead_task_name: null;
        lead_url: null;
        product_type: string;
        salepoint_assignment_date: string;
    }[];
};
export declare const mockKPI: {
    counters: {
        id: number;
        title: string;
        value: null;
    }[];
    link: string;
};
export declare const leadCardsMockAssigned: {
    count_all: number;
    count_overdue: number;
    data: {
        assignment_deadline: string;
        contact_person_name: string;
        contact_person_phone: null;
        feedback_fixation_deadline: string;
        lead_channel: string;
        lead_company_name: string;
        lead_created_date: string;
        lead_task_name: null;
        lead_url: null;
        product_type: string;
        salepoint_assignment_date: string;
    }[];
};
export declare const dummyData: {
    count_actual: number;
    data: never[];
};
export declare const leadNotificationsDummy: {
    lead_notifications: {
        unallocated: {
            count_actual: number;
            data: never[];
        };
        assigned: {
            count_actual: number;
            data: never[];
        };
    };
};
export declare const leadNotifications: {
    lead_notifications: {
        unallocated: {
            count_all: number;
            count_overdue: number;
            data: {
                assignment_deadline: string;
                contact_person_name: string;
                contact_person_phone: null;
                feedback_fixation_deadline: string;
                lead_channel: string;
                lead_company_name: string;
                lead_created_date: string;
                lead_task_name: null;
                lead_url: null;
                product_type: string;
                salepoint_assignment_date: string;
            }[];
        };
        assigned: {
            count_all: number;
            count_overdue: number;
            data: {
                assignment_deadline: string;
                contact_person_name: string;
                contact_person_phone: null;
                feedback_fixation_deadline: string;
                lead_channel: string;
                lead_company_name: string;
                lead_created_date: string;
                lead_task_name: null;
                lead_url: null;
                product_type: string;
                salepoint_assignment_date: string;
            }[];
        };
    };
};
