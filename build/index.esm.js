import React, { createContext, useContext, useState, useCallback, useEffect, createElement } from 'react';
import { LeadCard, PriorityPanelItem, PriorityPanel, NotificationPanel } from 'ui-kit-stream';
import { Base64 } from 'js-base64';
import PageVisibility from 'react-page-visibility';
import Cookies from 'js-cookie';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

//@ts-ignore
var plus1Day = 1000 * 60 * 60 * 24;
var plus10Minites = 1000 * 60 * 10;
var plus15Minites = 1000 * 60 * 15;
var unallocatedError = {
    //@ts-ignore
    lead_created: new Date(Date.parse(new Date()) - plus1Day)
        .toISOString()
        .slice(0, 23),
    salepoint_assignment_date: new Date().toISOString().slice(0, 23),
    assignment_deadline: new Date(new Date().getTime() - 1000)
        .toISOString()
        .slice(0, 23),
    work_starting_deadline: new Date(new Date().getTime() - 1000)
        .toISOString()
        .slice(0, 23),
    feedback_fixation_deadline: new Date(new Date().getTime() + 10000)
        .toISOString()
        .slice(0, 23),
};
var unallocatedWarning = {
    salepoint_assignment_date: new Date().toISOString().slice(0, 23),
    //@ts-ignore
    lead_created: new Date(Date.parse(new Date()) - plus10Minites)
        .toISOString()
        .slice(0, 23),
    assignment_deadline: new Date(new Date().getTime() + 5000)
        .toISOString()
        .slice(0, 23),
    work_starting_deadline: new Date(new Date().getTime() + 5000)
        .toISOString()
        .slice(0, 23),
    feedback_fixation_deadline: new Date(new Date().getTime() + plus10Minites + 5000)
        .toISOString()
        .slice(0, 23),
};
var unallocatedSuccess = {
    lead_created: new Date().toISOString().slice(0, 23),
    assignment_deadline: new Date(new Date().getTime() + plus10Minites)
        .toISOString()
        .slice(0, 23),
    work_starting_deadline: new Date(new Date().getTime() + plus10Minites)
        .toISOString()
        .slice(0, 23),
    feedback_fixation_deadline: new Date(new Date().getTime() + plus15Minites)
        .toISOString()
        .slice(0, 23),
};
var mockKPI = {
    counters: [
        {
            id: Math.random(),
            title: "KPI",
            //@ts-ignore
            value: null,
        },
    ],
    link: "",
};
var dummyData = {
    count_actual: 0,
    data: [],
};
var leadNotificationsDummy = {
    lead_notifications: {
        unallocated: dummyData,
        assigned: dummyData,
    },
};

var TokenContext = createContext({
    token: "",
    isLeader: false,
    isPageActive: false,
});

var isServer = typeof window === 'undefined';
var baseUrl = "";
//@ts-ignore
if (process.env.NODE_ENV === "development") {
    baseUrl = "";
}
//@ts-ignore
if (process.env.NODE_ENV === "production") {
    baseUrl = isServer ? '/' : window.location.protocol + "//" + window.location.hostname + "/";
}
var useHttp = function () {
    var token = useContext(TokenContext).token;
    var _a = useState(false), loading = _a[0], setLoading = _a[1];
    var _b = useState(null), error = _b[0], setError = _b[1];
    var request = useCallback(function (url, method, body, signal) {
        if (method === void 0) { method = "GET"; }
        if (body === void 0) { body = null; }
        if (signal === void 0) { signal = null; }
        return __awaiter(void 0, void 0, void 0, function () {
            var headers, response, e_1;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        setLoading(true);
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, , 4]);
                        if (body) {
                            body = JSON.stringify(body);
                        }
                        headers = (_a = {},
                            _a["Content-Type"] = "application/json",
                            _a);
                        if (token)
                            headers["Authorization"] = "Bearer " + token;
                        return [4 /*yield*/, fetch("" + baseUrl + "api/lead-service/lead" + url, {
                                method: method,
                                body: body,
                                headers: headers,
                                signal: signal,
                            })];
                    case 2:
                        response = _b.sent();
                        setLoading(false);
                        return [2 /*return*/, response];
                    case 3:
                        e_1 = _b.sent();
                        setLoading(false);
                        setError(e_1.message);
                        throw e_1;
                    case 4: return [2 /*return*/];
                }
            });
        });
    }, []);
    var clearError = useCallback(function () { return setError(null); }, []);
    return { loading: loading, request: request, error: error, clearError: clearError };
};

var styles = {"leadsWrapper":"lead-cards-module_leadsWrapper__1MTvt"};

var LeadCards = function (_a) {
    var data = _a.data;
    return (React.createElement("div", { className: styles.leadsWrapper }, data.map(function (leadCard) { return (React.createElement(LeadCard, __assign({ key: Math.random() }, leadCard))); })));
};

var PanelItemContainer = function (_a) {
    var leadsData = _a.leadsData, link = _a.link, counters = _a.counters;
    var _b = useState(false), show = _b[0], setShow = _b[1];
    var onMouseEnter = function () { return setShow(true); };
    var onMouseLeave = function () { return setShow(false); };
    var onHandleClick = function (e) {
        e.preventDefault();
        var pathname = window.location.pathname;
        if (!pathname.endsWith(link)) {
            window.location.href = window.origin + link;
        }
    };
    return (React.createElement(PriorityPanelItem
    //@ts-ignore
    , { 
        //@ts-ignore
        onMouseEnter: onMouseEnter, onMouseLeave: onMouseLeave, link: link, data: counters, onHandleClick: onHandleClick }, show && (React.createElement(LeadCards
    //@ts-ignore
    , { 
        //@ts-ignore
        onMouseEnter: onMouseEnter, onMouseLeave: onMouseLeave, data: leadsData }))));
};

var links = {
    leader: {
        assign: "Назначить",
        reassign: "Переназначить",
        takeLead: "Взять в работу",
    },
    mok: {
        takeLead: "Взять в работу",
    },
};
var panelTitles = {
    unallocated: "Нераспределенные лиды",
    overdue: "Просрочено",
    assignedToLeader: "Назначенные",
    assignedToMOK: "Назначенные мне",
    KPI: "KPI",
};
var panelTypes = {
    success: "success",
    attention: "attention",
};
var timerOverdue = {
    overdue: "Просрочено",
};
var timerTypes = {
    success: "success",
    warning: "warning",
    error: "error",
};

const getTokenPayload = (token) =>
  JSON.parse(Base64.decode(token.split(".")[1]));

const leadingZero = (num) => String(num).padStart(2, "0");

const getMs = (timestamp) => Date.parse(timestamp);

var transformTimeCreated = function (time) {
    var date = new Date(time);
    if (new Date().setHours(0) > Date.parse(time))
        return (leadingZero(date.getDate()) +
            "." +
            leadingZero(date.getMonth() + 1) +
            "." +
            date.getFullYear() +
            "  " +
            leadingZero(date.getHours()) +
            ":" +
            leadingZero(date.getMinutes()));
    return leadingZero(date.getHours()) + ":" + leadingZero(date.getMinutes());
};
//@ts-ignore
var getTimeLeft = function (deadline) { return Date.parse(deadline) - Date.parse(new Date()); };
var transformTimeLeft = function (timeLeft) {
    var date = new Date(timeLeft);
    if (timeLeft <= 0) {
        return timerOverdue.overdue;
    }
    return leadingZero(date.getMinutes()) + ":" + leadingZero(date.getSeconds());
};
var getTimerType = function (overdueTimestamp, warningTimestamp) {
    var now = new Date();
    switch (true) {
        //@ts-ignore
        case now <= getMs(warningTimestamp):
            return timerTypes.success;
        //@ts-ignore
        case now <= getMs(overdueTimestamp):
            return timerTypes.warning;
        default:
            return timerTypes.error;
    }
};
var getTitleType = function (overdueTimestamp) {
    var nowMs = getMs(new Date());
    var overdueMs = getMs(overdueTimestamp);
    if (nowMs >= overdueMs) {
        return timerTypes.error;
    }
};
var getTitle = function (isAssigned, isLeader) {
    if (isAssigned) {
        if (isLeader) {
            return panelTitles.assignedToLeader;
        }
        if (typeof window === 'undefined') {
            return panelTitles.assignedToMOK;
        }
        return window.innerWidth > 1024 ? panelTitles.assignedToMOK : panelTitles.assignedToLeader;
    }
    return panelTitles.unallocated;
};
var getCounters = function (counters, isAssigned, isLeader) {
    var data = [];
    data.push({
        //@ts-ignore
        id: Math.random(),
        //@ts-ignore
        title: getTitle(isAssigned, isLeader),
        //@ts-ignore
        value: counters.count_all ? counters.count_all : 0,
        //@ts-ignore
        type: panelTypes.success,
    });
    if (counters.count_overdue) {
        data.push({
            //@ts-ignore
            id: Math.random(),
            //@ts-ignore
            title: panelTitles.overdue,
            //@ts-ignore
            value: counters.count_overdue,
            //@ts-ignore
            isWarning: true,
        });
    }
    return data;
};
var updateLeads = function (data) {
    return data.map(function (lead) {
        if (lead.timeLeft <= 0) {
            return lead;
        }
        return __assign(__assign({}, lead), { timeLeft: lead.timeLeft - 1000, timerType: getTimerType(lead.overdueTimestamp + "Z", lead.warningTimestamp + "Z"), titleType: getTitleType(lead.overdueTimestamp + "Z"), timer: transformTimeLeft(lead.timeLeft - 1000) });
    });
};
var changeTimeLeft = function (data) {
    return data.map(function (leads) { return (__assign(__assign({}, leads), { data: updateLeads(leads.data) })); });
};
var getLinks = function (isAssigned, isLeader) {
    var linksData = [];
    if (isLeader) {
        linksData = [
            {
                //@ts-ignore
                id: Math.random(),
                //@ts-ignore
                title: isAssigned ? links.leader.reassign : links.leader.assign,
                //@ts-ignoreleader
                href: "#",
            },
            //@ts-ignore
            { id: Math.random(), title: links.leader.takeLead, href: "#" },
        ];
    }
    else {
        //@ts-ignore
        linksData = [{ id: Math.random(), title: links.mok.takeLead, href: "#" }];
    }
    return linksData;
};
var getLeadCardTitle = function (title) {
    switch (true) {
        case title === 'Банковская гарантия':
            return 'Банковскую гарантию';
        default:
            return title;
    }
};
var transformLeads = function (data, isLeader) {
    var transformedData = [];
    var _loop_1 = function (key) {
        var leadsData = data[key].data;
        var isAssigned = key === "assigned";
        var counters = getCounters(data[key], isAssigned, isLeader);
        var transformedLeads = leadsData.map(function (lead) {
            var timeLeft = getTimeLeft(lead.feedback_fixation_deadline + "Z");
            return {
                id: lead.lead_id,
                title: "\u0417\u0430\u044F\u0432\u043A\u0430 \u043D\u0430 " + getLeadCardTitle(lead.product_type),
                created: transformTimeCreated(lead.salepoint_assignment_date + "Z"),
                companyName: lead.lead_company_name,
                fullName: lead.contact_person_name,
                phone: lead.contact_person_phone,
                leadChannel: lead.lead_channel,
                timeLeft: timeLeft,
                timerType: getTimerType(lead.feedback_fixation_deadline + "Z", lead.assignment_deadline + "Z"),
                titleType: getTitleType(lead.feedback_fixation_deadline + "Z"),
                links: getLinks(isAssigned, isLeader),
                branch: isAssigned && isLeader && lead.sale_point_name,
                performer: isAssigned && isLeader && lead.assignee_employee_name,
                timer: transformTimeLeft(timeLeft),
                overdueTimestamp: lead.feedback_fixation_deadline,
                warningTimestamp: lead.assignment_deadline,
            };
        });
        transformedData.push({
            data: transformedLeads,
            counters: counters,
            link: "",
            id: Math.random(),
        });
    };
    for (var key in data) {
        _loop_1(key);
    }
    return transformedData;
};

var transformToasts = function (_a, isLeader) {
    var data = _a.lead_notifications.data;
    return data.map(function (toast) {
        return {
            id: toast.id,
            links: getLinks$1(toast.lead_url, isLeader, toast.event),
            isSuccess: toast.event === "TASK_CREATED" || toast.event === "TASK_ASSIGNED",
            content: {
                isNew: toast.event === "TASK_CREATED" || toast.event === "TASK_ASSIGNED",
                type: getType(toast.event, toast.assignee_employee_name, toast.sale_point_name, isLeader),
                companyName: toast.product_type + " \u2014 " + toast.company_name,
            },
        };
    });
};
var getLinks$1 = function (links, isLeader, event) {
    var linksData = [{}];
    if (isLeader) {
        if (event === "ASSIGNED_TASK_OVERDUE_FOR_START_OF_WORK") {
            linksData.push({
                content: "Переназначить",
                href: "#",
                key: Math.random(),
            });
        }
        else {
            linksData.push({ content: "Назначить", href: "#", key: Math.random() });
        }
    }
    linksData.push({
        content: "Взять в работу",
        href: "#",
        key: Math.random(),
    });
    // TODO переделать когда данные с бека будут приходить
    return linksData.slice(1, linksData.length);
};
var getType = function (event, employeeName, salePointName, isLeader) {
    var template = "";
    if (isLeader) {
        if (event === "ASSIGNED_TASK_OVERDUE_FOR_START_OF_WORK") {
            return (template = "\u041B\u0438\u0434 \u043D\u0430 " + salePointName + " \u2014 " + employeeName);
        }
        else {
            return (template = "\u041B\u0438\u0434 \u043D\u0430 \u0422\u041F");
        }
    }
    else {
        if (event === "TASK_ASSIGNED" ||
            event === "OVERDUE_FOR_START_OF_WORK_TASK_ASSIGNED") {
            return (template = "Назначен лид");
        }
        if (event === "ASSIGNED_TASK_OVERDUE_FOR_START_OF_WORK") {
            return (template = "Мой лид");
        }
        else {
            return (template = "Лид на ТП");
        }
    }
};

// import "ui-kit-stream/build/ui-kit-stream.css";
var AlertPanel = function () {
    var controller = typeof window === 'undefined' ? null : new AbortController();
    var signal = controller ? controller.signal : null;
    var isLeader = useContext(TokenContext).isLeader;
    var request = useHttp().request;
    var _a = useState(transformLeads(leadNotificationsDummy.lead_notifications, isLeader)
    // transformLeads(leadNotifications.lead_notifications, isLeader)
    ), transformedData = _a[0], setTransformedData = _a[1];
    var _b = useState(false), startTimer = _b[0], setStartTimer = _b[1];
    var subscribe = function () { return __awaiter(void 0, void 0, void 0, function () {
        var response, data, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 9, , 12]);
                    return [4 /*yield*/, request("/tasks", 'GET', null, signal)];
                case 1:
                    response = _a.sent();
                    if (!!response.ok) return [3 /*break*/, 4];
                    return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 10000); })];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, subscribe()];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 8];
                case 4: return [4 /*yield*/, response.json()];
                case 5:
                    data = _a.sent();
                    setTransformedData(transformLeads(data.lead_notifications, isLeader));
                    setStartTimer(true);
                    return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 10000); })];
                case 6:
                    _a.sent();
                    return [4 /*yield*/, subscribe()];
                case 7:
                    _a.sent();
                    _a.label = 8;
                case 8: return [3 /*break*/, 12];
                case 9:
                    e_1 = _a.sent();
                    console.log("fetch leads error", e_1);
                    return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 10000); })];
                case 10:
                    _a.sent();
                    return [4 /*yield*/, subscribe()];
                case 11:
                    _a.sent();
                    return [3 /*break*/, 12];
                case 12: return [2 /*return*/];
            }
        });
    }); };
    useEffect(function () {
        subscribe();
        return function () { return controller ? controller.abort() : undefined; };
    }, []);
    var _c = useState(false), timer = _c[0], setTimer = _c[1];
    useEffect(function () {
        if (startTimer) {
            var timeout_1 = setTimeout(function () {
                setTransformedData(changeTimeLeft(transformedData));
                setTimer(!timer);
            }, 1000);
            return function () { return clearTimeout(timeout_1); };
        }
    }, [startTimer, timer, transformedData]);
    return (React.createElement(PriorityPanel, null,
        React.createElement(PanelItemContainer, { key: Math.random(), link: "/ui/results/", counters: mockKPI.counters, leadsData: [] }),
        transformedData.map(function (leads, idx) { return (React.createElement(PanelItemContainer, { key: idx, link: leads.link, counters: leads.counters, leadsData: leads.data })); })));
};

var styles$1 = {"wrapper":"toasts-popup-module_wrapper__1O2e1","show":"toasts-popup-module_show__33vNU","toastsPopup":"toasts-popup-module_toastsPopup__FAG7s","appearence":"toasts-popup-module_appearence__3XqJW","hideAfterHover":"toasts-popup-module_hideAfterHover__DWm5h","hiding":"toasts-popup-module_hiding__14Rtb"};

var ToastsPopup = function () {
    var controller = typeof window === 'undefined' ? null : new AbortController();
    var signal = controller ? controller.signal : null;
    var _a = useContext(TokenContext), isLeader = _a.isLeader, isPageActive = _a.isPageActive;
    var _b = useState(""), className = _b[0], setClassName = _b[1];
    var _c = useState([]), transformedToasts = _c[0], setTransformedToasts = _c[1];
    var _d = useState(0), currentIndex = _d[0], setCurrentIndex = _d[1];
    var _e = useState(null), currentToast = _e[0], setCurrentToast = _e[1];
    var request = useHttp().request;
    var subscribe = function () { return __awaiter(void 0, void 0, void 0, function () {
        var response, res, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 9, , 14]);
                    setTransformedToasts([]);
                    return [4 /*yield*/, request("/notifications?limit=10", 'GET', null, signal)];
                case 1:
                    response = _a.sent();
                    if (!!response.ok) return [3 /*break*/, 4];
                    return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 10000); })];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, subscribe()];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 8];
                case 4: return [4 /*yield*/, response.json()];
                case 5:
                    res = _a.sent();
                    setTransformedToasts(transformToasts(res, isLeader));
                    if (!!res.lead_notifications.data.length) return [3 /*break*/, 8];
                    return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 10000); })];
                case 6:
                    _a.sent();
                    return [4 /*yield*/, subscribe()];
                case 7:
                    _a.sent();
                    _a.label = 8;
                case 8: return [3 /*break*/, 14];
                case 9:
                    e_1 = _a.sent();
                    if (!(e_1.name == "AbortError")) return [3 /*break*/, 10];
                    return [3 /*break*/, 13];
                case 10:
                    console.log("fetch toasts error", e_1);
                    return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 10000); })];
                case 11:
                    _a.sent();
                    return [4 /*yield*/, subscribe()];
                case 12:
                    _a.sent();
                    _a.label = 13;
                case 13: return [3 /*break*/, 14];
                case 14: return [2 /*return*/];
            }
        });
    }); };
    useEffect(function () {
        // setTransformedToasts(transformToasts(toastsMock, isLeader))
        subscribe();
        return function () { return controller ? controller.abort() : undefined; };
    }, []);
    useEffect(function () {
        var _a;
        setCurrentToast(transformedToasts[currentIndex]);
        setClassName(styles$1.toastsPopup);
        //@ts-ignore
        if ((_a = transformedToasts[currentIndex]) === null || _a === void 0 ? void 0 : _a.id) {
            request("/notification-user-notified", "POST", {
                //@ts-ignore
                lead_notifications_id: transformedToasts[currentIndex].id
            });
        }
    }, [transformedToasts, currentIndex]);
    var onAnimationEndHandler = function () {
        setClassName("");
        if (currentIndex === transformedToasts.length - 1) {
            subscribe();
            setCurrentIndex(0);
        }
        else {
            setCurrentIndex(currentIndex + 1);
        }
    };
    return (React.createElement("div", { className: styles$1.wrapper }, currentToast && className && (React.createElement("div", { 
        //@ts-ignore
        id: currentToast.id, onAnimationEnd: onAnimationEndHandler, className: "" + className, onMouseEnter: function () { return setClassName(styles$1.show); }, onMouseLeave: function () { return setClassName(styles$1.hideAfterHover); } },
        React.createElement(NotificationPanel, __assign({ onClick: function () { return onAnimationEndHandler(); } }, currentToast))))));
};

// import "ui-kit-stream/build/ui-kit-stream.css";
var Toasts = function () { return React.createElement(ToastsPopup, null); };

var styles$2 = {"wrapper":"panel-module_wrapper__1r0y2"};

var Panel = function () {
    var token = Cookies.get('access_token');
    var payloadData = token ? getTokenPayload(token) : dummyData$1;
    var permissions = payloadData.permissions, employeeType = payloadData.attributes.employee_type;
    var _a = useState(true), isPageActive = _a[0], setIsPageActive = _a[1];
    var hasAccess = permissions.includes("lk_emp.leads.allow_all");
    var isLeader = employeeType !== "EMPLOYEE";
    var handleVisibilityChange = function (isVisible) {
        setIsPageActive(isVisible);
    };
    if (hasAccess) {
        return (createElement(TokenContext.Provider, { value: { token: token, isLeader: isLeader, isPageActive: isPageActive } },
            createElement(PageVisibility, { onChange: handleVisibilityChange }, isPageActive && (createElement("div", { className: styles$2.wrapper },
                createElement(AlertPanel, null),
                createElement(Toasts, null))))));
    }
    return null;
};
var dummyData$1 = {
    permissions: [],
    attributes: { employee_type: "" },
};

export { AlertPanel, Panel, Toasts };
//# sourceMappingURL=index.esm.js.map
