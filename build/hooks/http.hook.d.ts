export declare const useHttp: () => {
    loading: boolean;
    request: (url: any, method?: any, body?: any, signal?: any) => Promise<Response>;
    error: null;
    clearError: () => void;
};
