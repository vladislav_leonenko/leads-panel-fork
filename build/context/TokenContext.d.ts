/// <reference types="react" />
export declare const TokenContext: import("react").Context<{
    token: string;
    isLeader: boolean;
    isPageActive: boolean;
}>;
