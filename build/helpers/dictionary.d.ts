export declare const links: {
    leader: {
        assign: string;
        reassign: string;
        takeLead: string;
    };
    mok: {
        takeLead: string;
    };
};
export declare const panelTitles: {
    unallocated: string;
    overdue: string;
    assignedToLeader: string;
    assignedToMOK: string;
    KPI: string;
};
export declare const panelTypes: {
    success: string;
    attention: string;
};
export declare const timerOverdue: {
    overdue: string;
};
export declare const timerTypes: {
    success: string;
    warning: string;
    error: string;
};
export declare const titleTypes: {
    success: string;
    error: string;
};
export declare const channel: Map<string, string>;
