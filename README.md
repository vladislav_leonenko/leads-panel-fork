# frkk-leads-panel-frontend

# Установка

Добавляем в корень репозитория файл .npmrc с необходимым registry и кредами авторизации

Устанавливаем пакет путем добавления зависимости в файл package.json (в настоящее время невозможно установить пакет стандартной командой npm install "имя пакета")

```
$ "dependencies": {
$   ...
$   "frkk-leads-panel-frontend": "^1.0.55",
$   ...
$ },
```

Выполняем команду npm install в консоли

# Подключение к проекту

К основному файлу index.[js|tsx] подключаем стили

```
$ frkk-leads-panel-frontend/build/leads-stream.css
```

# Использование

Импортируем компонент Panel из пакета 'frkk-leads-panel-frontend'.
Внимание, логика Panel построена таким образом, что она показывается только при наличии в permissions атрибута "lk_emp.leads.allow_all".
Если в токене отсутствует эта роль, то панель показана НЕ будет.
Панель самостоятельно ходит в куки за токеном и ищет в нем необходимую роль.

```
import { Panel } from 'frkk-leads-panel-frontend';

render() {
    return (
        <Panel />
    );
  }
```
