import pkg from "./package.json";
import replace from "@rollup/plugin-replace";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "rollup-plugin-typescript2";
import excludeDependenciesFromBundle from "rollup-plugin-exclude-dependencies-from-bundle";
import sourceMaps from "rollup-plugin-sourcemaps";
import postcss from "rollup-plugin-postcss";
import autoprefixer from "autoprefixer";
import cssnano from "cssnano";
require("dotenv").config();

const getPluginsConfig = (isProduction) => {
  return [
    sourceMaps(),
    typescript({
      clean: !!process.env.CLEAN_BUILD,
    }),
    postcss({
      modules: true,
      extract: "leads-stream.css",
      minimize: true,
      plugins: [
        autoprefixer({
          overrideBrowserslist: ["last 3 versions"],
          cascade: false,
        }),
        cssnano(),
      ],
    }),
    replace({
      "process.env.NODE_ENV": isProduction ? "process.env.NODE_ENV" : "",
    }),
    commonjs(),
    excludeDependenciesFromBundle(),
  ];
};

export default (isProduction) => ({
  input: "src/index.ts",
  output: [
    {
      file: pkg.main,
      format: "cjs",
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: "es",
      sourcemap: true,
    },
  ],
  external: ["react", "react-dom", "react-proptypes", "prop-types"],
  plugins: getPluginsConfig(isProduction),
});
