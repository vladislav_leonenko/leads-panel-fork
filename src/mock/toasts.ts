const linksData = [
  { content: "Назначить", href: "#", key: Math.random() },
  { content: "Взять в работу", href: "#", key: Math.random() },
];

const contentNew = {
  assignee_employee_name: null,
  company_name: "Желтый",
  event: "ASSIGNED_TASK_OVERDUE_FOR_START_OF_WORK",
  id: "413d94ce-070c-4aad-bb90-b0721154dc44",
  lead_url: null,
  product_type: "SCS",
  sale_point_name: "Точка продаж 100",
};
const contentOVerdue = {
  assignee_employee_name: null,
  company_name: "Зеленый",
  event: "TASK_ASSIGNED",
  id: "413d94ce-070c-4aad-bb90-b0721154dc44",
  lead_url: null,
  product_type: "SCS",
  sale_point_name: "Точка продаж 100",
};

const generateMocks = () => {
  let data = [];
  for (let i = 0; i < 50; i++) {
    //@ts-ignore
    data.push(contentNew);
    //@ts-ignorec
    data.push(contentOVerdue);
  }
  return { lead_notifications: { data } };
};

export const toastsMock = generateMocks();
