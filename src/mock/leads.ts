//@ts-ignore
const randomInteger = (min, max) => {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
};

const plus1Day = 1000 * 60 * 60 * 24;
const plus10Minites = 1000 * 60 * 10;
const plus15Minites = 1000 * 60 * 15;

const unallocatedError = {
  //@ts-ignore
  lead_created: new Date(Date.parse(new Date()) - plus1Day)
    .toISOString()
    .slice(0, 23),
  salepoint_assignment_date: new Date().toISOString().slice(0, 23),
  assignment_deadline: new Date(new Date().getTime() - 1000)
    .toISOString()
    .slice(0, 23),
  work_starting_deadline: new Date(new Date().getTime() - 1000)
    .toISOString()
    .slice(0, 23),
  feedback_fixation_deadline: new Date(new Date().getTime() + 10000)
    .toISOString()
    .slice(0, 23),
};
const unallocatedWarning = {
  salepoint_assignment_date: new Date().toISOString().slice(0, 23),
  //@ts-ignore
  lead_created: new Date(Date.parse(new Date()) - plus10Minites)
    .toISOString()
    .slice(0, 23),
  assignment_deadline: new Date(new Date().getTime() + 5000)
    .toISOString()
    .slice(0, 23),
  work_starting_deadline: new Date(new Date().getTime() + 5000)
    .toISOString()
    .slice(0, 23),
  feedback_fixation_deadline: new Date(
    new Date().getTime() + plus10Minites + 5000
  )
    .toISOString()
    .slice(0, 23),
};

const unallocatedSuccess = {
  lead_created: new Date().toISOString().slice(0, 23),
  assignment_deadline: new Date(new Date().getTime() + plus10Minites)
    .toISOString()
    .slice(0, 23),
  work_starting_deadline: new Date(new Date().getTime() + plus10Minites)
    .toISOString()
    .slice(0, 23),
  feedback_fixation_deadline: new Date(new Date().getTime() + plus15Minites)
    .toISOString()
    .slice(0, 23),
};

export const leadCardsMockUnallocated = {
  count_all: randomInteger(5, 10),
  count_overdue: randomInteger(0, 4),
  data: [
    {
      assignment_deadline: unallocatedError.assignment_deadline,
      contact_person_name: "Иванов Иван Иванович",
      contact_person_phone: null,
      feedback_fixation_deadline: unallocatedError.feedback_fixation_deadline,
      lead_channel: "Дистанционный канал",
      lead_company_name: "Название компании 1",
      lead_created_date: unallocatedError.lead_created,
      lead_task_name: null,
      lead_url: null,
      product_type: "Банковская гарантия",
      salepoint_assignment_date: unallocatedError.salepoint_assignment_date,
    },
    {
      assignment_deadline: unallocatedError.assignment_deadline,
      contact_person_name: "Иванов Иван Иванович",
      contact_person_phone: null,
      feedback_fixation_deadline: unallocatedError.feedback_fixation_deadline,
      lead_channel: "Дистанционный канал",
      lead_company_name: "Название компании 1",
      lead_created_date: unallocatedError.lead_created,
      lead_task_name: null,
      lead_url: null,
      product_type: "РКО",
      salepoint_assignment_date: unallocatedError.salepoint_assignment_date,
    },
  ],
};

export const mockKPI = {
  counters: [
    {
      id: Math.random(),
      title: "KPI",
      //@ts-ignore
      value: null,
      //@ts-ignore
    },
  ],
  link: "",
};

export const leadCardsMockAssigned = {
  count_all: randomInteger(5, 3),
  count_overdue: randomInteger(1, 2),
  data: [
    {
      assignment_deadline: unallocatedError.assignment_deadline,
      contact_person_name: "Иванов Иван Иванович",
      contact_person_phone: null,
      feedback_fixation_deadline: unallocatedError.feedback_fixation_deadline,
      lead_channel: "Дистанционный канал",
      lead_company_name: "Название компании 1",
      lead_created_date: unallocatedError.lead_created,
      lead_task_name: null,
      lead_url: null,
      product_type: "РКО",
      salepoint_assignment_date: unallocatedError.salepoint_assignment_date,
    },
  ],
};

export const dummyData = {
  count_actual: 0,
  data: [],
};

export const leadNotificationsDummy = {
  lead_notifications: {
    unallocated: dummyData,
    assigned: dummyData,
  },
};

export const leadNotifications = {
  lead_notifications: {
    unallocated: leadCardsMockUnallocated,
    assigned: leadCardsMockAssigned,
  },
};
