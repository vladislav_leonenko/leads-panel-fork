import { createContext } from "react";

export const TokenContext = createContext({
  token: "",
  isLeader: false,
  isPageActive: false,
});
