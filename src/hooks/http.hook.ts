import { useState, useCallback, useContext } from "react";
import { TokenContext } from "../context/TokenContext";

const isServer = typeof window === 'undefined';
let baseUrl = "";
//@ts-ignore
if (process.env.NODE_ENV === "development") {
  baseUrl = "";
}
//@ts-ignore
if (process.env.NODE_ENV === "production") {
  baseUrl = isServer ? '/' : `${window.location.protocol}//${window.location.hostname}/`;
}

export const useHttp = () => {
  const { token } = useContext(TokenContext);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const request = useCallback(
    async (url, method = "GET", body = null, signal = null) => {
      setLoading(true);
      try {
        if (body) {
          body = JSON.stringify(body);
        }
        const headers = {
          ["Content-Type"]: "application/json",
        };
        if (token) headers["Authorization"] = `Bearer ${token}`;

        const response = await fetch(
          `${baseUrl}${"api/lead-service/lead"}${url}`,
          {
            method,
            body,
            headers,
            signal: signal,
          }
        );

        setLoading(false);
        return response;
      } catch (e) {
        setLoading(false);
        setError(e.message);
        throw e;
      }
    },
    []
  );

  const clearError = useCallback(() => setError(null), []);

  return { loading, request, error, clearError };
};
