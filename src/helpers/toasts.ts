export const transformToasts = ({ lead_notifications: { data } }, isLeader) => {
  return data.map((toast) => {
    return {
      id: toast.id,
      links: getLinks(toast.lead_url, isLeader, toast.event),
      isSuccess:
        toast.event === "TASK_CREATED" || toast.event === "TASK_ASSIGNED",
      content: {
        isNew:
          toast.event === "TASK_CREATED" || toast.event === "TASK_ASSIGNED",
        type: getType(
          toast.event,
          toast.assignee_employee_name,
          toast.sale_point_name,
          isLeader
        ),
        companyName: `${toast.product_type} — ${toast.company_name}`,
      },
    };
  });
};

const getLinks = (links, isLeader, event) => {
  const linksData = [{}];
  if (isLeader) {
    if (event === "ASSIGNED_TASK_OVERDUE_FOR_START_OF_WORK") {
      linksData.push({
        content: "Переназначить",
        href: "#",
        key: Math.random(),
      });
    } else {
      linksData.push({ content: "Назначить", href: "#", key: Math.random() });
    }
  }
  linksData.push({
    content: "Взять в работу",
    href: "#",
    key: Math.random(),
  });
  // TODO переделать когда данные с бека будут приходить
  return linksData.slice(1, linksData.length);
};

const getType = (event, employeeName, salePointName, isLeader) => {
  let template = ``;
  if (isLeader) {
    if (event === "ASSIGNED_TASK_OVERDUE_FOR_START_OF_WORK") {
      return (template = `Лид на ${salePointName} — ${employeeName}`);
    } else {
      return (template = `Лид на ТП`);
    }
  } else {
    if (
      event === "TASK_ASSIGNED" ||
      event === "OVERDUE_FOR_START_OF_WORK_TASK_ASSIGNED"
    ) {
      return (template = "Назначен лид");
    }
    if (event === "ASSIGNED_TASK_OVERDUE_FOR_START_OF_WORK") {
      return (template = "Мой лид");
    } else {
      return (template = "Лид на ТП");
    }
  }
};
