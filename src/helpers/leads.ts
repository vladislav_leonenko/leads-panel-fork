import {
  panelTitles,
  timerOverdue,
  timerTypes,
  panelTypes,
  links,
} from "./dictionary";
import { leadingZero, getMs } from "../utils";

const transformTimeCreated = (time) => {
  const date = new Date(time);
  if (new Date().setHours(0) > Date.parse(time))
    return (
      leadingZero(date.getDate()) +
      "." +
      leadingZero(date.getMonth() + 1) +
      "." +
      date.getFullYear() +
      "  " +
      leadingZero(date.getHours()) +
      ":" +
      leadingZero(date.getMinutes())
    );
  return leadingZero(date.getHours()) + ":" + leadingZero(date.getMinutes());
};
//@ts-ignore
const getTimeLeft = (deadline) => Date.parse(deadline) - Date.parse(new Date());

export const transformTimeLeft = (timeLeft) => {
  const date = new Date(timeLeft);
  if (timeLeft <= 0) {
    return timerOverdue.overdue;
  }
  return leadingZero(date.getMinutes()) + ":" + leadingZero(date.getSeconds());
};

const getTimerType = (overdueTimestamp, warningTimestamp) => {
  const now = new Date();
  switch (true) {
    //@ts-ignore
    case now <= getMs(warningTimestamp):
      return timerTypes.success;
    //@ts-ignore
    case now <= getMs(overdueTimestamp):
      return timerTypes.warning;
    default:
      return timerTypes.error;
  }
};

const getTitleType = (overdueTimestamp) => {
  const nowMs = getMs(new Date());
  const overdueMs = getMs(overdueTimestamp);
  if (nowMs >= overdueMs) {
    return timerTypes.error;
  }
};

const getTitle = (isAssigned, isLeader) => {
  if (isAssigned) {
    if (isLeader) {
      return panelTitles.assignedToLeader;
    }
    if (typeof window === 'undefined') {
      return panelTitles.assignedToMOK
    }
    return window.innerWidth > 1024 ? panelTitles.assignedToMOK : panelTitles.assignedToLeader;
  }
  return panelTitles.unallocated;
};

const getCounters = (counters, isAssigned, isLeader) => {
  const data = [];
  data.push({
    //@ts-ignore
    id: Math.random(),
    //@ts-ignore
    title: getTitle(isAssigned, isLeader),
    //@ts-ignore
    value: counters.count_all ? counters.count_all : 0,
    //@ts-ignore
    type: panelTypes.success,
  });
  if (counters.count_overdue) {
    data.push({
      //@ts-ignore
      id: Math.random(),
      //@ts-ignore
      title: panelTitles.overdue,
      //@ts-ignore
      value: counters.count_overdue,
      //@ts-ignore
      isWarning: true,
    });
  }
  return data;
};

const updateLeads = (data) =>
  data.map((lead) => {
    if (lead.timeLeft <= 0) {
      return lead;
    }
    return {
      ...lead,
      timeLeft: lead.timeLeft - 1000,
      timerType: getTimerType(
        `${lead.overdueTimestamp}Z`,
        `${lead.warningTimestamp}Z`
      ),
      titleType: getTitleType(`${lead.overdueTimestamp}Z`),
      timer: transformTimeLeft(lead.timeLeft - 1000),
    };
  });

export const changeTimeLeft = (data) =>
  data.map((leads) => ({
    ...leads,
    data: updateLeads(leads.data),
  }));

const getLinks = (isAssigned, isLeader) => {
  let linksData = [];
  if (isLeader) {
    linksData = [
      {
        //@ts-ignore
        id: Math.random(),
        //@ts-ignore
        title: isAssigned ? links.leader.reassign : links.leader.assign,
        //@ts-ignoreleader
        href: "#",
      },
      //@ts-ignore
      { id: Math.random(), title: links.leader.takeLead, href: "#" },
    ];
  } else {
    //@ts-ignore
    linksData = [{ id: Math.random(), title: links.mok.takeLead, href: "#" }];
  }
  return linksData;
};

const getLeadCardTitle = (title) => {
  switch(true) {
    case title === 'Банковская гарантия':
      return 'Банковскую гарантию';
    default:
      return title;
  }
}

export const transformLeads = (data, isLeader) => {
  const transformedData: any = [];
  for (let key in data) {
    const { data: leadsData } = data[key];
    const isAssigned = key === "assigned";
    const counters = getCounters(data[key], isAssigned, isLeader);
    const transformedLeads = leadsData.map((lead) => {
      const timeLeft = getTimeLeft(`${lead.feedback_fixation_deadline}Z`);
      return {
        id: lead.lead_id,
        title: `Заявка на ${getLeadCardTitle(lead.product_type)}`,
        created: transformTimeCreated(`${lead.salepoint_assignment_date}Z`),
        companyName: lead.lead_company_name,
        fullName: lead.contact_person_name,
        phone: lead.contact_person_phone,
        leadChannel: lead.lead_channel,
        timeLeft: timeLeft,
        timerType: getTimerType(
          `${lead.feedback_fixation_deadline}Z`,
          `${lead.assignment_deadline}Z`
        ),
        titleType: getTitleType(`${lead.feedback_fixation_deadline}Z`),
        links: getLinks(isAssigned, isLeader),
        branch: isAssigned && isLeader && lead.sale_point_name,
        performer: isAssigned && isLeader && lead.assignee_employee_name,
        timer: transformTimeLeft(timeLeft),
        overdueTimestamp: lead.feedback_fixation_deadline,
        warningTimestamp: lead.assignment_deadline,
      };
    });
    transformedData.push({
      data: transformedLeads,
      counters: counters,
      link: "",
      id: Math.random(),
    });
  }
  return transformedData;
};
