export const links = {
  leader: {
    assign: "Назначить",
    reassign: "Переназначить",
    takeLead: "Взять в работу",
  },
  mok: {
    takeLead: "Взять в работу",
  },
};
export const panelTitles = {
  unallocated: "Нераспределенные лиды",
  overdue: "Просрочено",
  assignedToLeader: "Назначенные",
  assignedToMOK: "Назначенные мне",
  KPI: "KPI",
};
export const panelTypes = {
  success: "success",
  attention: "attention",
};

export const timerOverdue = {
  overdue: "Просрочено",
};

export const timerTypes = {
  success: "success",
  warning: "warning",
  error: "error",
};

export const titleTypes = {
  success: "success",
  error: "error",
};

export const channel = new Map([
  ["INCOMING_TRAFFIC", "Входящий трафик"],
  ["REMOTE_CHANNEL", "Дистанционный канал"],
  ["CRM_CAMPAIGN", "CRM кампания"],
  ["MARKET", "Рынок"],
  ["RECOMMENDATION", "Рекомендация"],
]);
