import { Base64 } from "js-base64";

export const getTokenHeader = (token) =>
  JSON.parse(Base64.decode(token.split(".")[0]));

export const getTokenPayload = (token) =>
  JSON.parse(Base64.decode(token.split(".")[1]));

export const getTokenSignature = (token) =>
  JSON.parse(Base64.decode(token.split(".")[2]));

export const leadingZero = (num) => String(num).padStart(2, "0");

export const getMs = (timestamp) => Date.parse(timestamp);

