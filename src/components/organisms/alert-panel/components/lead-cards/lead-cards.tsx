import React from "react";
import { LeadCard } from "ui-kit-stream";
//@ts-ignore
import styles from "./lead-cards.module.css";

export const LeadCards = ({ data }) => (
  <div className={styles.leadsWrapper}>
    {data.map((leadCard) => (
      <LeadCard key={Math.random()} {...leadCard} />
    ))}
  </div>
);
