import React, { useState } from "react";
import { PriorityPanelItem } from "ui-kit-stream";
import { LeadCards } from "../../components";

export const PanelItemContainer = ({ leadsData, link, counters }) => {
  const [show, setShow] = useState(false);

  const onMouseEnter = () => setShow(true);
  const onMouseLeave = () => setShow(false);
  
  const onHandleClick = (e) => {
    e.preventDefault();
    let pathname = window.location.pathname
    if (!pathname.endsWith(link)) {
      window.location.href = window.origin + link
    }
  }

  return (
    <PriorityPanelItem
      //@ts-ignore
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      link={link}
      data={counters}
      onHandleClick={onHandleClick}
    >
      {show && (
        <LeadCards
          //@ts-ignore
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          data={leadsData}
        />
      )}
    </PriorityPanelItem>
  );
};
