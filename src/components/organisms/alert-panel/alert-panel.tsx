import React, { useEffect, useState, useContext } from "react";
import {
  mockKPI,
  leadNotificationsDummy,
  leadNotifications,
} from "../../../mock";
import { PriorityPanel } from "ui-kit-stream";
import { useHttp } from "../../../hooks";
import { PanelItemContainer } from "./containers";
import { TokenContext } from "../../../context/TokenContext";
import { transformLeads, changeTimeLeft } from "../../../helpers";
// import "ui-kit-stream/build/ui-kit-stream.css";

export const AlertPanel = () => {
  let controller = typeof window === 'undefined' ? null : new AbortController();
  const signal = controller ? controller.signal : null;
  
  const { isLeader } = useContext(TokenContext);
  const { request } = useHttp();

  const [transformedData, setTransformedData] = useState(
    transformLeads(leadNotificationsDummy.lead_notifications, isLeader)
    // transformLeads(leadNotifications.lead_notifications, isLeader)
  );
  const [startTimer, setStartTimer] = useState(false);

  const subscribe = async () => {
    try {
      let response = await request(
        `/tasks`,
        'GET',
        null,
        signal
      );
      if (!response.ok) {
        await new Promise((resolve) => setTimeout(resolve, 10000));
        await subscribe();
      } else {
        let data = await response.json();
        setTransformedData(transformLeads(data.lead_notifications, isLeader));
        setStartTimer(true);
        await new Promise((resolve) => setTimeout(resolve, 10000));
        await subscribe();
      }
    } catch (e) {
      console.log("fetch leads error", e);
      await new Promise((resolve) => setTimeout(resolve, 10000));
      await subscribe();
    }
  };

  useEffect(() => {
    subscribe();
    return () => controller ? controller.abort() : undefined
  }, []);

  const [timer, setTimer] = useState(false);
  useEffect(() => {
    if (startTimer) {
      const timeout = setTimeout(() => {
        setTransformedData(changeTimeLeft(transformedData));
        setTimer(!timer);
      }, 1000);
      return () => clearTimeout(timeout);
    }
  }, [startTimer, timer, transformedData]);
  return (
    <PriorityPanel>
      <PanelItemContainer
        key={Math.random()}
        link={"/ui/results/"}
        counters={mockKPI.counters}
        leadsData={[]}
      />
      {transformedData.map((leads, idx) => (
        <PanelItemContainer
          key={idx}
          link={leads.link}
          counters={leads.counters}
          leadsData={leads.data}
        />
      ))}
    </PriorityPanel>
  );
};
