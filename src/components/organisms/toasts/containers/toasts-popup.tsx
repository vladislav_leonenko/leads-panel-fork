import React, { useEffect, useState, useContext } from "react";
import { TokenContext } from "../../../../context/TokenContext";
import { NotificationPanel } from "ui-kit-stream";
import { useHttp } from "../../../../hooks";
import { transformToasts } from "../../../../helpers";
import { toastsMock } from "../../../../mock";
//@ts-ignore
import styles from "./toasts-popup.module.css";

export const ToastsPopup = () => {
  let controller = typeof window === 'undefined' ? null : new AbortController();
  const signal = controller ? controller.signal : null;
  const { isLeader, isPageActive } = useContext(TokenContext);

  const [className, setClassName] = useState("");
  const [transformedToasts, setTransformedToasts] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [currentToast, setCurrentToast] = useState(null);

  const { request } = useHttp();
  const subscribe = async () => {
    try {
      setTransformedToasts([]);
      let response = await request(
        `/notifications?limit=10`,
        'GET',
        null,
        signal
      );
      if (!response.ok) {
        await new Promise((resolve) => setTimeout(resolve, 10000));
        await subscribe();
      } else {
        let res = await response.json();
        setTransformedToasts(transformToasts(res, isLeader));
        if (!res.lead_notifications.data.length) {
          await new Promise((resolve) => setTimeout(resolve, 10000));
          await subscribe();
        }
      }
    } catch (e) {
      if (e.name == "AbortError") {
        // обработать ошибку от вызова abort()
        // alert("Прервано!");
      } else {
        console.log("fetch toasts error", e);
        await new Promise((resolve) => setTimeout(resolve, 10000));
        await subscribe();
      }
    }
  };

  useEffect(() => {
    // setTransformedToasts(transformToasts(toastsMock, isLeader))
    subscribe();
    return () => controller ? controller.abort() : undefined
  }, []);

  useEffect(() => {
    setCurrentToast(transformedToasts[currentIndex]);
    setClassName(styles.toastsPopup);
    //@ts-ignore
    if (transformedToasts[currentIndex]?.id) {
      request(`/notification-user-notified`, "POST", {
        //@ts-ignore
        lead_notifications_id: transformedToasts[currentIndex].id
      });
    }
  }, [transformedToasts, currentIndex]);

  const onAnimationEndHandler = () => {
    setClassName("");
    if (currentIndex === transformedToasts.length - 1) {
      subscribe();
      setCurrentIndex(0);
    } else {
      setCurrentIndex(currentIndex + 1);
    }
  };

  return (
    <div className={styles.wrapper}>
      {currentToast && className && (
        <div
          //@ts-ignore
          id={currentToast.id}
          onAnimationEnd={onAnimationEndHandler}
          className={`${className}`}
          onMouseEnter={() => setClassName(styles.show)}
          onMouseLeave={() => setClassName(styles.hideAfterHover)}
        >
          {/* @ts-ignore */}
          <NotificationPanel
            onClick={() => onAnimationEndHandler()}
            {...currentToast}
          />
        </div>
      )}
    </div>
  );
};
