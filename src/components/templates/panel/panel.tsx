import * as React from "react";
import PageVisibility from "react-page-visibility";
import Cookies from 'js-cookie'
import { TokenContext } from "../../../context/TokenContext";
import { getTokenPayload } from "../../../utils";
import { AlertPanel, Toasts } from "../../organisms";
//@ts-ignore
import styles from "./panel.module.css";

export const Panel = () => {
  const token = Cookies.get('access_token')
  const payloadData = token ? getTokenPayload(token) : dummyData;
  const {
    permissions,
    attributes: { employee_type: employeeType },
  } = payloadData;

  const [isPageActive, setIsPageActive] = React.useState(true);
  const hasAccess = permissions.includes("lk_emp.leads.allow_all");
  const isLeader = employeeType !== "EMPLOYEE";

  const handleVisibilityChange = (isVisible) => {
    setIsPageActive(isVisible);
  };

  if (hasAccess) {
    return (
      <TokenContext.Provider value={{ token, isLeader, isPageActive }}>
        <PageVisibility onChange={handleVisibilityChange}>
          {isPageActive && (
            <div className={styles.wrapper}>
              <AlertPanel />
              <Toasts />
            </div>)
          }
        </PageVisibility>
      </TokenContext.Provider>
    );
  }
  return null;
};

const dummyData = {
  permissions: [],
  attributes: { employee_type: "" },
};
